# test 16 digit 7-segment module with TM1640 controller on ESP32

from machine import Pin
from tm1640 import *
from time import sleep

d = TM1640(Pin(4), Pin(15))
print("5-digit number, default brightness")
d.dec(12345)
sleep(2)
print("brightness 3")
d.brightness(3)
sleep(2)
print("brightness 7")
d.brightness(7)
print("string of 16 chars")
d.string("0123456789ABCDEF")
sleep(2)
print("large positive decimal value")
d.dec(1234567890123456)
sleep(2)
print("large negative decimal value")
d.number(-987654321098765)
sleep(2)
print("large hex value")
d.hex(0xFEDCBA9876543210)
sleep(2)
print("4 times 3-char dec numbers")
d.clear()
for i in range(4):
   d.dec((i+1)**3, 4, i*4)
   sleep(1)
print("4 times 3-char hex numbers")
d.clear()
for i in range(4):
   d.hex((i+1)**3, 4, i*4)
   sleep(1)
sleep(3)
print("scroll long string")
d.scroll("0123456789ABCDEFghijklmnopqrstuvwxyz-*")
print("Clear")
d.clear()
print("power off")
d.power(False)

