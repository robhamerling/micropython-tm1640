# micropython-tm1640

Micropython Library for 16 digits 7-segment displays controlled by a TM1640

Author: Rob Hamerling, Copyright (c) 2018..2018 all rights reserved.

Sample tested with an ESP32 board.

![demo](TM1640_TM1638_display.jpg)

Class TM1640(clk, din, brightness=7)
   - clk: Pin object for clock signal
   - din: Pin object for data-in signal
   - brightness: value in range 0..7

Methods:

   - power(val=None)

     Enable (val=True) or disable (val=False).

   - brightness(val=None)

     Set brightness: val 0..7.

   - clear()

     All segments of all digits off.

   - dec(num, width=16, pos=0, leading_zeroes=False)

     Display a value in decimal notation, right aligned,
     of specified width, at specified position, without leading zeroes (default).

   - hex(val, width=16, pos=0, leading_zeroes=True)

     Display a value in hexadecimal notation, right aligned
     of specified width, at specified position, with leading zeroes (default).

   - string(string, pos=0)

     Display a string, starting at a given offset.

   - scroll(string, delay=250)

     Display a (long) string, scrolling from right to left,
     with a specified speed (default .25 seconds per character).

   - temperature(num, pos=0)

     Display a temperature value ((degrees C) at a given position.

   - humidity(num, pos=4)

     Display a relative humidity at a given position.

