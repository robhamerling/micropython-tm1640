# tm1640.py
"""
MicroPython TM1640 driver for 16 digits 7-segment display
Author: Rob Hamerling, Copyright (c) 2018..2018 all rights reserved.
License: MIT
Notes: Derived from tm1638.py of Mike Causer
       - removed code related to read
       - removed code related to write of individual leds
       - changed _byte(): first din, then clk up-down
                          CLK pulse width set to 1 us (> 400 ns)
       - changed _write(): using auto address increment (performance)
       - removed segments(): duplicate of _write()
       - changed number(), show(), scroll(), etc: 16 digits (was 8)
                 scroll(): added a trailing 0x00 to clear display
                           made 'segments' a list (modified data assignment)
       - changed encode_char(): (hex-)digits first (most likely first: performance)
       - changed encode_char() and encode_string(): name preceded with underscore
       - renamed show() to string(): name more in line with hex, number, etc.
                        added show as alias for backward compatibility
       - renamed number() to dec() for consistency with hex(), and
                        added an alias 'number' for backward compatibility
       - removed encode_digit(): _encode_char() called instead
       - changed TM1640_DELAY to 1 us (was 10 us) (see also TM1640 datasheet)
       - added some functional enhancements to methods dec() and hex()
       - shuffled code such as separate 'private' and 'public' methods
"""

from micropython import const
from machine import Pin
from time import sleep_us, sleep_ms

TM1640_CMD_DATA  = const(64)  # 0x40 data command
TM1640_CMD_ADDR  = const(192) # 0xC0 address command
TM1640_CMD_DISP  = const(128) # 0x80 display control command
TM1640_DSP_ON    = const(8)   # 0x08 display on
TM1640_FIXED     = const(4)   # 0x04 fixed address mode

TM1640_DOT       = const(0x80) # segment of decimal point

TM1640_DELAY     = const(1)   # us delay between clk/din pulses
TM1640_CLK_PULSE = const(1)   # us clock pulse width (<= 400 ns)

# value limitations
TM1640_NUM_DIGITS = const(16)
TM1640_MAX_INT    = const(9999999999999999)
TM1640_MIN_INT    = const(-999999999999999)
TM1640_MAX_HEX    = const(0xFFFFFFFFFFFFFFFF)


# 0-9, a-z, blank, dash, star
_SEGMENTS = bytearray(b'\x3F\x06\x5B\x4F\x66\x6D\x7D\x07\x7F\x6F\x77\x7C\x39\x5E\x79\x71\x3D\x76\x06\x1E\x76\x38\x55\x54\x3F\x73\x67\x50\x6D\x78\x3E\x1C\x2A\x76\x6E\x5B\x00\x40\x63')

class TM1640(object):
    """Library for the TM1640 LED display driver.
       Arguments: Pin objects for clk and din, initial brightness
    """
    def __init__(self, clk, din, brightness=7):
        self.clk = clk     # Pin objects
        self.din = din

        self.clk.init(Pin.OUT, value=1)
        self.din.init(Pin.OUT, value=1)

        if not (0 <= brightness <= 7):
            raise ValueError("Brightness out of range")
        self._brightness = brightness & 0x07

        self._on = TM1640_DSP_ON
        self.clear()

        self.number = self.dec    # ) aliases for backward compatibility
        self.show = self.string   # )

    def _start(self):
        """ prepare for a transmission """
        self.din(0)
        sleep_us(TM1640_DELAY)
        self.clk(0)
        sleep_us(TM1640_DELAY)

    def _stop(self):
        """ end of a transmission """
        self.din(0)
        sleep_us(TM1640_DELAY)
        self.clk(1)
        sleep_us(TM1640_DELAY)
        self.din(1)

    def _byte(self, b):
        """ transfer all bits of a byte """
        for i in range(8):
            self.din((b >> i) & 1)
            self.clk(1)
            sleep_us(TM1640_CLK_PULSE)
            self.clk(0)
            sleep_us(TM1640_CLK_PULSE)

    def _command(self, cmd):
        """ send a command """
        self._start()
        self._byte(cmd)
        self._stop()

    def _write_data_cmd(self):
        """ send data command: automatic address increment, normal mode """
        self._command(TM1640_CMD_DATA)

    def _write_dsp_ctrl(self):
        # display command: display on, set brightness (auto addr increment)
        self._command(TM1640_CMD_DISP | self._on | self._brightness)

    def _set_address(self, addr=0):
        # address command: select address for next digit
        self._byte(TM1640_CMD_ADDR | (addr & 0x0F))

    def _encode_char(self, char):
        """ Convert a character 0-9, a-z, space, dash or star to a segment """
        o = ord(char)
        if (48 <= o <= 57):
            return _SEGMENTS[o-48] # 0-9
        if (o == 32):
            return _SEGMENTS[36] # space
        if (o == 42):
            return _SEGMENTS[38] # star/degrees
        if (o == 45):
            return _SEGMENTS[37] # dash
        if (65 <= o <= 90):
            return _SEGMENTS[o-55] # uppercase A-Z
        if (97 <= o <= 122):
            return _SEGMENTS[o-87] # lowercase a-z
        raise ValueError("Character out of range: {:d} '{:s}'".format(o, chr(o)))

    def _encode_string(self, string):
        """Convert a string containing 0-9, A-Z, a-z, space, dash, star to a bytearray
        of segment patterns, matching the length of the source string.
        Dots are merged with the previous character.
        """
        segments = bytearray(len(string.replace('.','')))
        j = 0
        for i in range(len(string)):
            if ((string[i] == '.') and (j > 0)):
                segments[j-1] |= TM1640_DOT             # concat dot to previous digit
                continue
            segments[j] = self._encode_char(string[i])
            j += 1
        return segments

    def _write(self, data, pos=0):
        """ Write data (supposedly segment patterns) to display
            byte by byte, left to right, starting at specified position
        """
        if not (0 <= pos < TM1640_NUM_DIGITS):
            raise ValueError("Position out of range")
        self._write_dsp_ctrl()      # set display mode
        self._write_data_cmd()      # select data mode
        self._start()
        self._set_address(pos)      # set start position
        for b in data:
            self._byte(b)           # data byte
        self._stop()


    """ ===  P U B L I C   M E T H O D S  === """

    def power(self, val=None):
        """ Power up, power down or check status """
        if val is None:
            return self._on == TM1640_DSP_ON
        self._on = TM1640_DSP_ON if val else 0
        self._write_dsp_ctrl()

    def brightness(self, val=None):
        """ Set the display brightness 0..7.
            brightness 0 = 1/16th pulse width
            brightness 7 = 14/16th pulse width
        """
        if val is None:
            return self._brightness
        if not (0 <= val <= 7):
            raise ValueError("Brightness out of range")
        self._brightness = val & 0x07
        self._write_dsp_ctrl()

    def clear(self):
        """ Write binary zeros to all digits """
        self._write([0] * TM1640_NUM_DIGITS)

    def dec(self, num, width=TM1640_NUM_DIGITS, pos=0, leading_zeroes=False):
        """Display a decimal number, right aligned, no leading zeros."""
        num = max(TM1640_MIN_INT, min(num, TM1640_MAX_INT))    # limited value
        if leading_zeroes:
           fmt = "{0:>0" + str(width) + "d}"
        else:
           fmt = "{0:>" + str(width) + "d}"
        self.string(fmt.format(num), pos)

    def hex(self, val, width=TM1640_NUM_DIGITS, pos=0, leading_zeroes=True):
        """ Display a hex value, right aligned, leading zeros """
        val &= TM1640_MAX_HEX                                  # limited value
        if leading_zeroes:
           fmt = "{0:>0" + str(width) + "x}"
        else:
           fmt = "{0:>" + str(width) + "x}"
        self.string(fmt.format(val & TM1640_MAX_HEX), pos)

    #def float(self, num):
    #    # needs more work
    #    string = '{0:>9f}'.format(num)
    #    self._write(self._encode_string(string[0:9]))

    def string(self, string, pos=0):
        """Displays a string starting at given offset
           Truncate string when it exceeds display width.
        """
        segments = self._encode_string(string)
        self._write(segments[ : TM1640_NUM_DIGITS - pos], pos)

    def scroll(self, string, delay=250):
        """ Display a string, scrolling from the right to left, speed adjustable.
            String starts off-screen right and scrolls left until off-screen.
            When 'string' is a list then assume it contains segment patterns,
            otherwise transform 'string' to a list of patterns.
        """
        segments = string if isinstance(string, list) else list(self._encode_string(string))
        data = [0] * TM1640_NUM_DIGITS + segments + [0]
        for i in range(len(data)):
            self._write(data[0 + i : TM1640_NUM_DIGITS + i])
            sleep_ms(delay)

    def temperature(self, num, pos=0):
        """Displays 2 digit temperature followed by degrees C"""
        if (num < -9):
            self.string('lo', pos) # low
        elif (num > 99):
            self.string('hi', pos) # high
        else:
            self.string('{0: >2d}'.format(num), pos)
        self.string('*C', pos + 2) # degrees C

    def humidity(self, num, pos=4):
        """Displays 2 digit humidity followed by rh"""
        if (num < -9):
            self.string('lo', pos) # low
        elif (num > 99):
            self.string('hi', pos) # high
        else:
            self.string('{0: >2d}'.format(num), pos)
        self.string('rh', pos + 2) # relative humidity

